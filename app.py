import pandas_datareader.data as web
import datetime
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc


from alpha_vantage.timeseries import TimeSeries
from alpha_vantage.techindicators import TechIndicators
from alpha_vantage.sectorperformance import SectorPerformances
from alpha_vantage.cryptocurrencies import CryptoCurrencies
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import time
import os
from datetime import datetime
from datetime import date


# make plots bigger
matplotlib.rcParams['figure.figsize'] = (20.0, 10.0)
# set key var
with open('.key') as f: key = f.read()

# set user vars

download_symbols=['QQQ','SPY','GOOGL','GOLD','VTI','VGLT']

force_download_day = False
list_symbols=['SPY','VGLT','GOLD']
combo_deals = {'combo1':{'SPY':70,'VGLT':20,'GOLD':10},'combo2':{'SPY':30,'VGLT':30,'GOLD':40}}
start_date = "2015-01-01"
end_date = "now"

# convert now to timestamp
if end_date == "now":
    end_date = datetime.today().strftime('%Y-%m-%d')

# creates options list for dynamic inventory multiselect
import glob
list_files = glob.glob("data/day/*")
clean_files = []
for file in list_files:
    clean_files.append(file[9:-4])

ticker_options =[]
for ticker in sorted(clean_files):
    ticker_options.append({'label': ticker, 'value': ticker})

# https://www.bootstrapcdn.com/bootswatch/
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.SANDSTONE],
                meta_tags=[{'name': 'viewport',
                            'content': 'width=device-width, initial-scale=1.0'}]
                )

# Layout section: Bootstrap (https://hackerthemes.com/bootstrap-cheatsheet/)
# ************************************************************************

app.layout = dbc.Container([

    dbc.Row(
        dbc.Col(html.H1("ETF Dashboard",
                        className='text-center text-primary mb-4'),
                width=12)
    ),


    dbc.Row([
        dbc.Col([
            
            html.H3("Date picker", className='text-left text-primary mb-4'),

            dcc.DatePickerRange(
            id='my-date-picker-range',
            min_date_allowed=date(1995, 8, 5),
            max_date_allowed=datetime.today().strftime('%Y-%m-%d'),
            initial_visible_month=date(2010, 1, 1),
            end_date=datetime.today().strftime('%Y-%m-%d'),
            start_date=date(2010, 1, 1)
            )
        
            

    
    
            ],width={'size':5}),


        dbc.Col([html.H3("Invest dividends at rebalance date?", className='text-left text-primary mb-4'),dcc.Dropdown(
        id='add_divs',
        options=[
            {'label': 'Yes', 'value': 1},
            {'label': "No", 'value': 0}
        ],
        value=1
    )],width={'size':5}),
        dbc.Col([html.H3("Starting Amount", className='text-center text-primary mb-4'),dcc.Input(id="startkapitaal", type="number", placeholder="startkapitaal", debounce=True, value=100)],md=4),
        dbc.Col([html.H3("Amount to invest", className='text-center text-primary mb-4'),dcc.Input(id="stortbedrag", type="number", placeholder="stortbedrag", debounce=True, value=0)],md=4),
        dbc.Col([html.H3("Invest every X days", className='text-center text-primary mb-4'),dcc.Input(id="stortdagen", type="number", placeholder="stortdagen", debounce=True, value=30)],md=4),
        
    ],justify="around"),


    dbc.Row([
        
        dcc.Dropdown(
            id='list_symbols',
            options=ticker_options,
            value=['SPY', 'GOLD','VGLT'],
            multi=True
)


    ]),

    dbc.Row([
        dbc.Col([html.Div(id='output-container-date-picker-range')],
                                    width={'size':12})
    ])


])

@app.callback(
    dash.dependencies.Output('output-container-date-picker-range', 'children'),
    [dash.dependencies.Input('my-date-picker-range', 'start_date'),
     dash.dependencies.Input('my-date-picker-range', 'end_date'),
     dash.dependencies.Input('startkapitaal', 'value'),
     dash.dependencies.Input('stortbedrag', 'value'),
     dash.dependencies.Input('stortdagen', 'value'),
     dash.dependencies.Input('add_divs', 'value'),
     dash.dependencies.Input('list_symbols', 'value')])

def update_value(starttime,endtime,startkapitaal,stortbedrag,stortdagen,add_divs,list_symbols):


    combo_deals = {'combo1':{'ratios':{'SPY':0.7,'VGLT':0.2,'GOLD':0.1}},'combo2':{'ratios':{'SPY':0.6,'VGLT':0.4}}}
    print(combo_deals)
    # create list of koersen to add ------------------------------------------------------------------------------
    all_koersen = []
    all_koersen.extend(list_symbols)

    # add all tickers from combo_deals
    for k,v in combo_deals.items():
        for key in v['ratios'].keys():
            all_koersen.append(key)

    # make set of list and thus remove duplicates
    all_koersen = set(all_koersen)
    
    # add list symbols to combo deals
    for symbol in list_symbols:
        combo_deals[symbol+'-show'] = {'ratios':{symbol:1.0},'balance':0} 
    
    data = []
    list_of_df = []
    for ticker in all_koersen:
        df = pd.read_csv("./data/day/"+ticker+".csv",index_col="date", parse_dates=True)
        df = df.loc[starttime:endtime]
        df = df[['4. close',"7. dividend amount"]]
        devider = df.iat[0,0]
        df[ticker] = df["4. close"]*startkapitaal/devider
        df[ticker+'-div'] = df["7. dividend amount"]*startkapitaal/devider
        df = df[[ticker,ticker+'-div']]
        list_of_df.append(df)

    result = pd.concat(list_of_df, axis=1, join='outer')
    for comboname,combostats in combo_deals.items():
        # create empty column of floats
        result[comboname] = 0.0
        
        # create empty dict for counting
        count_shares = {}

        global div_count
        div_count = 0
        
        # fil dict with keys
        for key in combostats['ratios']:
            count_shares[key] = combostats['ratios'][key]
        
        # loop over rows, van begin tot einde
        days_left_invest = stortdagen

        for row in result.itertuples():  

            # investeer
            if days_left_invest == 0:
                result.at[row.Index,comboname] = result.at[row.Index,comboname] + stortbedrag
                


            for key in combostats['ratios']:
            
            # bereken "shares" per aandeel
            # combostats[key] -> percentage item als 0-1
            # result.at[row.Index,comboname] -> waarde combi
            # key -> ticker naam
            # result.at[row.Index,key] -> waarde losse tickers
            # count_shares[key] -> aantal ticker
            
                #bereken totale waarde, nu is het aantal share nog staande van de dag ervoor
                result.at[row.Index,comboname] = result.at[row.Index,comboname] + count_shares[key] * result.at[row.Index,key]

                div_count = div_count + count_shares[key] * result.at[row.Index,key+'-div']


            # herbalanceer
            if days_left_invest == 0:
                
                if add_divs: 
                    result.at[row.Index,comboname] = result.at[row.Index,comboname] + stortbedrag + div_count
                else:
                    result.at[row.Index,comboname] = result.at[row.Index,comboname] + stortbedrag
                for key in combostats['ratios']:       
                # recount, verzin hier nog een if statement voor
                    count_shares[key] = combostats['ratios'][key] * result.at[row.Index,comboname] / result.at[row.Index,key]
                
                days_left_invest = stortdagen + 1
                div_count = 0.0
                
            # reduce days left
            days_left_invest -= 1
 







    #for kolom in result:
    #    data.append({'x': list(result.index.strftime("%m-%d-%Y")), 'y': list(result[kolom]), 'type': 'line', 'name': kolom})
    for key in combo_deals.keys():
        data.append({'x': list(result.index.strftime("%m-%d-%Y")), 'y': list(result[key]), 'type': 'line', 'name': key})

    return dcc.Graph(
        id='example-graph',
        figure={
            'data': data,
            'layout': {
                'title': "Growth over time"
            }
        }
    )

if __name__ == '__main__':
    app.run_server(debug=True)